package com.adaptavist.tutorials.reportingexample.core;

/**
 * Interface that defines a Report
 */
public interface Report {

    /**
     * Returns the content of this report
     * 
     * @return the returned String will contain the HTML output of this report
     */
    public String generateReport();

    /**
     * Returns the report name
     * 
     * @return report name
     */
    public String getName();

    /**
     * Returns a text description of this report
     * 
     * @return report description
     */
    public String getDescription();

    /**
     * Returns the unique key to identify this report.
     * 
     * @return report key
     */
    public String getKey();
}

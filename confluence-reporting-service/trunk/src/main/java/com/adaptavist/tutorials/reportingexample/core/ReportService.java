package com.adaptavist.tutorials.reportingexample.core;

import java.util.List;

/**
 * The main API by which other plugins will interact with the reporting service provided by this plugin.
 * Reports defined by other plugins via their atlassian-plugin.xml are automatically detected and can be
 * accessed through classes that implement this interface.
 */
public interface ReportService {

    /**
     * Returns all Reports provided by the currently installed plugins on the Confluence instance.
     * @return a list of registered Reports
     */
    public List<Report> getAllReports();

    /**
     * Find a Report by its unique key. If no report is found with a matching key the method returns NULL.
     * Note that since there is no enforcement of uniqueness of the Report key that if more than such
     * key exists then which Report returned is decided by the implementing class.
     * @param key the key of the report to find.
     * @return report if found or null otherwise
     */
    Report getReportByKey(String key);
}

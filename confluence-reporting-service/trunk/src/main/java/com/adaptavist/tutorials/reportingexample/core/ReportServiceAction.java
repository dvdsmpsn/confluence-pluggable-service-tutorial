package com.adaptavist.tutorials.reportingexample.core;

import java.util.List;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import org.apache.commons.lang.StringUtils;

/**
 * Simple UI that lists all available Reports and a link to invoke their Report.generateReport() method and
 * return the output.
 */
public class ReportServiceAction extends ConfluenceActionSupport {

    private ReportService reportService;

    @Override
    public String doDefault() throws Exception {
        return SUCCESS;
    }

    /**
     * Velocity helper method so as not to expose the ReportService to the view layer.
     * @return list of all report types.
     */
    public List getReportTypes(){
        return reportService.getAllReports();
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }
}

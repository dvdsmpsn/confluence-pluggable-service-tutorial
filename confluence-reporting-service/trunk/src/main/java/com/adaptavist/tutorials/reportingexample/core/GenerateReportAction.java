package com.adaptavist.tutorials.reportingexample.core;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 *
 */
public class GenerateReportAction  extends ConfluenceActionSupport {

    private ReportService reportService;

    /** URL Parameter **/
    private String key;

    /** View variable **/
    private Report report;

    @Override
    public String execute() throws Exception {
        if(StringUtils.isBlank(key)) {
            throw new IllegalArgumentException("URL parameter key must not be blank");
        }
        report = reportService.getReportByKey(key);
        return SUCCESS;
    }

    /**
     * Used to inject the URL parameter 'key' in to the action
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Used in the velocity view template
     * @return the Report corresponding the the URL parameter 'key'
     */
    public Report getReport() {
        return report;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }
}


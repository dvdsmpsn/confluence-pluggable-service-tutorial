package com.adaptavist.tutorials.reportingexample.core;

import com.atlassian.plugin.PluginAccessor;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Default implementation of the ReportService. Rather than register the Reports this implementation dynamically looks
 * through all plugins that have a ReportDescriptor and returns all ReportDescriptors found. This has the advantage of
 * meaning this plugin need not store any state, such as a list of installed Reports, but at the cost of fetching
 * available types from the system each and everytime they are required. For example, the implementation of
 * DefaultReportService.getReportByKey() is particularly inefficient in this respect, particulary if called
 * multiple times in succession.
 */
public class DefaultReportService implements ReportService {

    private PluginAccessor pluginAccessor;

    /**
     * Returns all Reports provided by the currently installed plugins on the Confluence instance.
     * @return a list of registered Reports
     */
    public List<Report> getAllReports() {
        List<ReportDescriptor> reportDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(ReportDescriptor.class);
        List<Report> reportTypes = new ArrayList<Report>(reportDescriptors.size());
        for(ReportDescriptor descriptor : reportDescriptors) {
            reportTypes.add(descriptor.getModule());
        }
        return reportTypes;
    }

    /**
     * Find a Report by its unique key. If no report is found with a matching key the method returns NULL. If more than
     * one Report with the same key exists then the first instance found will be returned.
     * @param key the key of the report to find.
     * @return report if found or null otherwise
     */
    public Report getReportByKey(String key) {
        List<Report> reports = getAllReports();
        for(Report report : reports) {
            if(report.getKey().equals(key)){
                return report;
            }
        }
        throw new NoSuchElementException("Unable to find a report with key=" + key);
    }

    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }
}

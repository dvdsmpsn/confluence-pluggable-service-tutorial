package com.adaptavist.tutorials.reportingexample.core;

import com.atlassian.plugin.AutowireCapablePlugin;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;

/**
 * Defines the <report> XML element for use in the atlassian-plugin.xml
 */
public final class ReportDescriptor extends AbstractModuleDescriptor<Report> {

    /**
     * Returns the Report object represented by an instance of the <report> element
     * @return report defined by this ReportDescriptor instance.
     */
    public Report getModule() {
        return ((AutowireCapablePlugin) getPlugin()).autowire(getModuleClass());
    }
}

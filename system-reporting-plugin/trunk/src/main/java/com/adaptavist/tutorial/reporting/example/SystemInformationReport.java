package com.adaptavist.tutorial.reporting.example;

import com.adaptavist.tutorials.reportingexample.core.Report;
import com.atlassian.confluence.status.service.SystemInformationService;

/**
 * System information example report.
 * 
 * Provides a simple report printing out information from the {{@ SystemInformationService}}
 */
public class SystemInformationReport implements Report {
    
    protected final String REPORT_NAME = "System Information Report";
    protected final String REPORT_DESCRIPTION = "Provides Information about Confluence System Usage";
    protected final String REPORT_KEY = "com.adaptavist.tutorial.reporting.example.system-reporting-plugin.standard-report";
    
    SystemInformationService systemInformationService;
    
    public String getName(){
        return REPORT_NAME;
    }
    
    public String getDescription(){
        return REPORT_DESCRIPTION;
    }

    public String getKey() {
        return REPORT_KEY;
    }

    public String generateReport() {
        return "Global Spaces: " + systemInformationService.getUsageInfo().getGlobalSpaces() + 
            "<br/>Pages:" + systemInformationService.getUsageInfo().getCurrentContent();
    }

    
    public void setSystemInformationService(SystemInformationService systemInformationService) {
        this.systemInformationService = systemInformationService;
    }

}
